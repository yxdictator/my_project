import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
from sklearn.decomposition import IncrementalPCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from typing import Union
import copy
from sklearn.cluster import KMeans

# DataClean
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------


def default_cleaning(data: Union[list, np.ndarray, pd.Series]) -> pd.Series:
    """
    Default data cleaning mode, fill nan, kick extreme,  but not normalize

    Parameters
    ----------
    data: list, array, pandas Series

    Returns
    -------
    pandas Series
    """
    result = fillnan(data, 'front_fill')
    result = kick_extreme(result)
    # Normalize should be done after dimension.
    # result = self.normalize(result)
    return result


def fillnan(data: Union[list, np.ndarray, pd.Series],
            method: str) -> pd.Series:
    """
    Processing np.nan in data.

    Parameters
    ----------
    data: list, array, pandas Series
    method: str
        str with example{'drop','fill_by_mean','front_fill','back_fill'}

    Returns
    -------
    pandas Series
    """
    if method == 'drop':
        result = _fillna_drop(data)
    elif method == 'fill_by_mean':
        result = _fillna_mean(data)
    elif method == 'front_fill':
        result = _fillna_front(data)
    elif method == 'back_fill':
        result = _fillna_back(data)
    else:
        raise Exception('fill_na method not existed.')
    return result


def kick_extreme(data: Union[list, np.ndarray, pd.Series],
                 method: str = 'smart') -> pd.Series:
    """
    Remove the extremum in the factor.

    Parameters
    ----------
    data: list, array, pandas Series
    method: str
        str with example{'MAD','3sigma','percentile','smart','mad_tanh'}

    Returns
    -------
    pandas Series
    """

    if method == 'MAD':
        result = _kick_extreme_mad(data, n=3)
    elif method == '3sigma':
        result = _kick_extreme_3sigma(data)
    elif method == 'percentile':
        result = _kick_extreme_percentile(data)
    elif method == 'smart':
        result = _kick_extreme_smart(data)
    elif method == 'mad_tanh':
        result = _kick_extreme_mad_tanh(data, n=3)
    else:
        raise Exception('Kick extreme Method not existed！')
    return result


def normalize(data: Union[list, np.ndarray, pd.Series]) -> pd.Series:
    """
    Normalize to mean 0 and standard deviation 1.

    Parameters
    ----------
    data: list, array, pandas Series

    Returns
    -------
    pandas Series
    """
    data = _data_to_series(data)
    ave = np.nanmean(data)
    std = np.nanstd(data)
    ret = (data - ave) / std
    return ret


def kick_inf(data: Union[list, np.ndarray, pd.Series]) -> pd.Series:
    """
    Eliminate inf value, set inf or -inf to np.nan

    Parameters
    ----------
    data: list, array, pandas Series

    Returns
    -------
    pandas Series
    """
    data = _data_to_series(data)
    data[data == np.inf] = np.nan
    data[data == -np.inf] = np.nan
    return data


def _fillna_drop(data: Union[list, np.ndarray, pd.Series]) -> pd.Series:
    """
    Drop np.nan.

    Parameters
    ----------
    data: list, array, pandas Series

    Returns
    -------
    pandas Series
    """
    data = _data_to_series(data)
    result = data.dropna()
    return result


def _fillna_mean(data: Union[list, np.ndarray, pd.Series]) -> pd.Series:
    """
    Fill np.nan with mean of non-null values

    Parameters
    ----------
    data: list, array, pandas Series

    Returns
    -------
    pandas Series
    """
    data = _data_to_series(data)
    arr_remove = np.array([i for i in data.values if not np.isnan(i)])
    result = data.fillna(value=np.mean(arr_remove))
    return result


def _fillna_front(data: Union[list, np.ndarray, pd.Series]) -> pd.Series:
    """
    Fill np.nanwith the previous non-empty data.

    Parameters
    ----------
    data: list, array, pandas Series

    Returns
    -------
    pandas Series
    """
    data = _data_to_series(data)
    result = data.fillna(value=None, method='ffill', axis=0, limit=None)
    return result


def _fillna_back(data: Union[list, np.ndarray, pd.Series]) -> pd.Series:
    """
    Fill it with the following non-empty data.

    Parameters
    ----------
    data: list, array, pandas Series

    Returns
    -------
    pandas Series
    """
    data = _data_to_series(data)
    result = data.fillna(value=None, method='bfill', axis=0, limit=None)
    return result


def _kick_extreme_smart(data: Union[list, np.ndarray, pd.Series]) -> pd.Series:
    """
    Automatically select, default is MAD, if MAD appears the result is
    equal to 0, then use 3sigma

    Parameters
    ----------
    data: list, array, pandas Series

    Returns
    -------
    pandas Series
    """
    ret = _kick_extreme_mad(data)
    if ret.std(axis=0) == 0:
        ret = _kick_extreme_3sigma(data)
    return ret


def _kick_extreme_mad(
        data: Union[list, np.ndarray, pd.Series], n: float = 3) -> pd.Series:
    """
    MAD method is used to remove outliers.

    Parameters
    ----------
    data: list, array, pandas Series
    n: float

    Returns
    -------
    pandas Series
    """
    data = _data_to_series(data)
    data = data.apply(lambda x: float(x))
    median1 = np.nanmedian(data)
    median2 = np.nanmedian(np.abs(data - median1))
    upper = median1 + n * 1.4826 * median2
    lower = median1 - n * 1.4826 * median2
    result = data.clip(lower=lower, upper=upper)
    return result


def _kick_extreme_3sigma(
        data: Union[list, np.ndarray, pd.Series], n: float = 3) -> pd.Series:
    """
    3Sigma method is used to remove outliers.

    Parameters
    ----------
    data: list, array, pandas Series
    n: float

    Returns
    -------
    pandas Series
    """
    #
    """ After removing NaN, the mean value and standard deviation were
    obtained."""
    data = _data_to_series(data)
    ave = np.nanmean(data)
    std = np.nanstd(data)
    upper = ave + n * std
    lower = ave - n * std
    result = data.clip(lower=lower, upper=upper)
    return result


def _kick_extreme_percentile(data: Union[list,
                                         np.ndarray,
                                         pd.Series],
                             low: float = 0.05,
                             up: float = 0.95) -> pd.Series:
    """
    Percentile method is used to remove outliers.

    Parameters
    ----------
    data: list, array, pandas Series
    low: float
    up: float

    Returns
    -------
    pandas Series
    """
    data = _data_to_series(data)
    series_tmp = pd.Series([i for i in data if not np.isnan(i)])
    low_bound = series_tmp.quantile(low)
    up_bound = series_tmp.quantile(up)
    result = data.clip(lower=low_bound, upper=up_bound)
    return result


def _kick_extreme_mad_tanh(
        data: Union[list, np.ndarray, pd.Series], n: float = 3) -> pd.Series:
    """
    MAD_tanh method is used to remove outliers.

    Parameters
    ----------
    data: list, array, pandas Series
    n: float

    Returns
    -------
    pandas Series
    """
    df2 = _data_to_series(data)
    median = df2.median()
    new_median = ((df2 - median).abs()).median()
    max_range = median + 1.4826 * n * new_median
    min_range = median - 1.4826 * n * new_median
    df2[df2 > max_range] = max_range + new_median * \
        np.tanh((df2[
            df2 > max_range] - max_range) / new_median)
    df2[df2 < min_range] = min_range + new_median * \
        np.tanh((df2[
            df2 < min_range] - min_range) / new_median)
    return df2


def _data_to_series(data: Union[list, np.ndarray, pd.Series]) -> pd.Series:
    """
    Data is converted to Series format, and all None is converted to np.nan

    Parameters
    ----------
    data: list, array, pandas Series

    Returns
    -------
    pandas Series
    """
    if isinstance(data, pd.Series):
        data = data
    elif isinstance(data, np.ndarray):
        data = pd.Series(data=data, index=range(len(data)))
    elif isinstance(data, list):
        data = pd.Series(data=data, index=range(len(data)))
    elif isinstance(data, pd.DataFrame):
        if data.shape[1] == 1:
            data = pd.Series(data=data, index=data.index)
        else:
            raise Exception("The dimension of DataFrame should be 1")
    data = data.apply(lambda j: np.nan if j is None else j)
    return data


# DataDimension
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------


# Non supervision dimensionality reduction by PCA
def pca(data: np.ndarray, componets: int) -> np.ndarray:
    """
    Use PCA for dimension reduction.
    Tips:
    1 Command PCA is suitable for small-scale dimension reduction,for
    large-scale dimension reduction, please use IncrementalPCA
    2 The component must be larger than the dimension of the feature,PCA
    only seek data that contains as much information (Variance) as possible
    3 Reverse PCA is not always possible to recover all information

    Parameters
    ----------
    data: numpy ndarray
    componets: int

    Returns
    -------
    numpy ndarray
    """
    pca = PCA(n_components=componets)
    pca.fit(data)
    pca_data = pca.transform(data)
    # pca_inv = pca.inverse_transform(pca_data)
    return pca_data


def incremental_pca(data: np.ndarray, componet: int) -> np.ndarray:
    """
    Use PCA for large-scale dimension reduction.
    Tips:
    1 Command IncrementalPCA is suitable for large-scale dimension
    reduction

    Parameters
    ----------
    data: numpy ndarray
    componet: int

    Returns
    -------
    numpy ndarray
    """
    pca = IncrementalPCA(n_components=componet, batch_size=None)
    pca.fit(data)
    pca_data = pca.transform(data)
    # pca_inv = pca.inverse_transform(pca_data)
    return pca_data


# Supervision dimensionality reduction by LDA


def lda(data_x: np.ndarray, data_y: np.ndarray, componet: int) -> np.ndarray:
    """
    Use LDA in sklearn for dimension reduction. If there are n classification,
    *componet must below n-1

    Parameters
    ----------
    data_x: numpy ndarray
    data_y: numpy ndarray
    componet: The dimension number expected.

    Returns
    -------
    numpy ndarray
    """
    lda = LinearDiscriminantAnalysis(n_components=componet)
    lda.fit(data_x, data_y)
    lda_data = lda.transform(data_x)
    return lda_data


# Dimensionality reduction by correlation coefficient
def abs_corr_triumatrix(
        data: pd.DataFrame,
        method: str = 'pearson') -> pd.DataFrame:
    """
    ***Pass manual testing
    Obtain correlation coefficient matrix.
    Tips：
    1: Pearson correlation coefficient is used to measure whether two data
    sets are on the same line, that is, the correlation coefficient is
    calculated for linear data, and there will be errors for nonlinear data
    2: Kendall correlation coefficient is used to reflect the correlation
    of classified variables, i.e. correlation coefficients for disordered
    sequences, and non-normal distribution(classified variables)
    3:Spearman correlation coefficient is used in non-linear, non-normal
    distribution(non-linear, non-normal)

    Parameters
    ----------
    data: pandas DataFrame
        Element in DataFrame must be int or float, it can't be a str, np.nan, .
    method: str

    Returns
    -------
    pandas DataFrame
    """
    corr = data.corr(method=method, min_periods=1)
    abs_corr = corr.apply(lambda x: abs(x))
    list = abs_corr.values.tolist()
    mat = np.mat(list)
    up_tri_mat = np.triu(mat, k=1)
    new_list = up_tri_mat.tolist()
    triu_df = pd.DataFrame(new_list)
    return triu_df


def sort_abs_corr(triu_df: pd.DataFrame) -> list:
    """
    ***Pass manual testing
    Sort the absolute value of the correlation coefficient.

    Parameters
    ----------
    abs_corr: pandas DataFrame

    Returns
    -------
    list
    """
    new_list = triu_df.values.tolist()
    # change 2 dimension to 1 dimension list
    corr_list = [s for t in new_list for s in t]
    # Descending
    corr_list.sort(reverse=True)
    # print(dimension_list)
    corr_nonzero_list = [s for t in new_list for s in t if s > 0]
    # Descending
    corr_nonzero_list.sort(reverse=True)
    # print(dimension_nonzero_list)
    return corr_nonzero_list


def get_position(corr_value: float, df: pd.DataFrame) -> list:
    """
    ***Pass manual testing
    Returns the index of the element in the DataFrame df that is equal to
    corr_value.

    Parameters
    ----------
    corr_value: float
    df: pandas DataFrame

    Returns
    -------
    list with example:[(1,2),(5,6),(5,8)] here (5,8) is a tuple.
    """
    my_array = df.values
    position = np.where(my_array == corr_value)
    list_pair = list(zip(position[0], position[1]))
    return list_pair


def dimen_reduce_corr(
        triu_df: pd.DataFrame,
        corr_nonzero_list: list,
        reduce_n_dimension: int,
        feature_ic_asb_average: list) -> list:
    """
    ***Pass manual testing
    Dimension reduction by correlation coefficient.
    Tips:

    Parameters
    ----------
    triu_df: pandas DataFrame, obtain by triu_df=abs_corr_triumatrix(data)
    corr_nonzero_list: list, obtain by corr_nonzero_list=sort_abs_corr(triu_df)
    reduce_n_dimension: int, dimension number expected to delete
    feature_ic_asb_average: list with example[0.1,0.6], elements must be
    positive float below 1

    Returns
    -------
    list, elements must be positive int,  represents the dimension that will be
    removed.
    """
    new_list = copy.deepcopy(corr_nonzero_list)
    # Descending,
    new_list.sort(reverse=True)
    corr_max = 0

    if (not isinstance(reduce_n_dimension, type(
            int(reduce_n_dimension))) or reduce_n_dimension < 0):
        raise Exception('The value should be a positive integer!!!')
    if (reduce_n_dimension >= len(new_list)):
        raise Exception('Dimension reduction more than max length!!!')
    if corr_nonzero_list:
        pass
    else:
        raise Exception('The corr_nonzero_list should be not empty!!!')
    # Feature is located only by number.
    deleted_dimension = []
    # corr_nonzero_list is arranged in descending order,
    for corr_value in new_list:
        if reduce_n_dimension == 0:
            break
        temp_pair_list = get_position(corr_value, triu_df)
        for same_corr in temp_pair_list:
            if reduce_n_dimension == 0:
                break
            if (same_corr[0] in deleted_dimension) or (
                    same_corr[1] in deleted_dimension):
                pass
            else:
                corr_max = corr_value
                # delete the dimension with fewer absolute average IC
                if feature_ic_asb_average[same_corr[1]] > \
                        feature_ic_asb_average[same_corr[0]]:
                    deleted_dimension.append(same_corr[0])
                else:
                    deleted_dimension.append(same_corr[1])
                reduce_n_dimension = reduce_n_dimension - 1
    if reduce_n_dimension != 0:
        print(
            'in function:***dimen_reduct_corr***Failed to reduce the given '
            'reduce_n_dimension, but this is not a failure situation, there is just '
            'not that much dimension need to be reduced!')
    return [deleted_dimension, corr_max]


def reduce_dimension_by_corr(
        deleted_dimension: list,
        data: pd.DataFrame) -> pd.DataFrame:
    """
    ***Pass manual testing
    Reduces dimensions according to deleted_dimension.

    Parameters
    ----------
    deleted_dimension: list
    data: pandas DataFrame

    Returns
    -------
    pandas DataFrame
    """
    my_columns = data.columns
    select_col = [my_columns[i] for i in deleted_dimension]
    new_data = data.drop(select_col, axis=1)
    return new_data


def abs_icir(data_xy: pd.DataFrame) -> list:
    """
    ***Pass manual testing
    # One method is to use real ICIR, but the calculation is a little bit more
    # complicated. Another method is calculating the correlation coefficient
    # between the factor and the rate of return in all days as a fake ICIR. We
     use the fake ICIR here.

    Parameters
    ----------
    data_xy: pandas DataFrame

    Returns
    -------
    list
    """
    res = []
    for i in range(data_xy.shape[1] - 1):
        temp = data_xy.iloc[:, [i, -1]]
        corr_index = temp.corr()
        res.append(corr_index.iloc[1, 0])
    abs_res = list(map(lambda x: abs(x), res))
    return abs_res


def reduce_dimension_my_method(
        data_x: pd.DataFrame,
        data_y: pd.DataFrame,
        corr_proportion: float,
        lda_proportion: float,
        pca_proportion: float) -> np.ndarray:
    """
    Do reduction dimension by correlation firstly, then do reduction dimension
    by LDA and finally do reduction dimension by PCA. Do reduction dimension by
    correlation exist no limitation; then do reduction dimension by LDA for
    better classification or regression; finally, do reduction dimension by PCA
    to reduce the number of features on the basis of preserving information as
    much as possible.

    Parameters
    ----------
    data_x: pandas DataFrame
    data_y: pandas DataFrame
    corr_proportion: float, 0-1
    lda_proportion: float, 0-1
    pca_proportion: float, 0-1

    Returns
    -------
    numpy ndarray, data after dimensionality reduction
    """
    # Dimension reduction by correlation coefficient.
    # -------------------------------------------------------------------------
    # Obtain upper triangular correlation coefficient matrix.
    print('Star reduce_dimension_my_method')
    triu_df = abs_corr_triumatrix(data_x)
    # Decide reduction number
    n_dimension = int(triu_df.shape[1] * (1 - corr_proportion))
    # Sort absolute correlation.
    corr_nonzero_list = sort_abs_corr(triu_df)

    # Caculate fake ICIR for deleting dimension.
    data_x = data_x.reset_index(drop=True)
    data_y = data_y.reset_index(drop=True)
    data_xy = pd.concat([data_x, data_y], axis=1)
    feature_ic_asb_average = abs_icir(data_xy)
    # Decide drop which feature

    temp_dimen = dimen_reduce_corr(
        triu_df,
        corr_nonzero_list,
        n_dimension,
        feature_ic_asb_average)

    deleted_dimension = temp_dimen[0]
    max_corr = temp_dimen[1]
    print(max_corr, 'is the max correlation after doing dimension reduction! ')
    # Drop feature
    new_data = reduce_dimension_by_corr(deleted_dimension, data_x)
    # -------------------------------------------------------------------------
    # Dimension reduction by LDA further.
    # LDA lead feature below n classification!!!
    lda_reduct_num = int(lda_proportion * new_data.shape[1])
    new_data_array = new_data.values
    new_data_y = data_y.values
    lda_array = lda(new_data_array, new_data_y, lda_reduct_num)
    # -------------------------------------------------------------------------
    # Dimension reduction by PCA further.
    pca_reduct_num = int(pca_proportion * lda_array.shape[1])
    data_final_array = incremental_pca(lda_array, pca_reduct_num)
    return data_final_array


def clean_and_reduct_data(
        prop_test_day,
        y_clusters_num,
        corr_proportion,
        lda_proportion,
        pca_proportion,
        filename):
    """
    Escape data clean, time is too long, assume that the data has been cleaned.

    Parameters
    ----------
    prop_test_day
    y_clusters_num
    corr_proportion
    lda_proportion
    pca_proportion
    filename

    Returns
    -------

    """
    h5_store = pd.HDFStore(filename, mode='r')
    my_keys = h5_store.keys()
    data = h5_store.get(my_keys[0])
    print('The data is loaded!')
    # select all data
    data = data[data['TradingDay'] <= prop_test_day]
    # # tset must delete ******************************************************
    # data=data.iloc[:1000,:]
    # *** data clean: fill nan, kick extreme

    # *************************************************************************
    # time too long, escape dataclearn
    # data = data.apply(default_cleaning, axis=1)
    print('default_cleaning over!')
    # Dimension reduction
    data_day_code = data.iloc[:, :3]
    data_x = data.iloc[:, 2:-1]
    data_y = data.iloc[:, -1]
    # Cluster y by K-means
    data_y_array = data_y.values
    data_y_reshape = data_y_array.reshape(-1, 1)
    kmeans = KMeans(n_clusters=y_clusters_num).fit(data_y_reshape)
    cluster_y = kmeans.labels_
    cluster_y = pd.DataFrame(cluster_y)
    # Dimension reduction
    print('The  reduce_dimension_my_method star!')
    data_final_array = reduce_dimension_my_method(
        data_x, cluster_y, corr_proportion, lda_proportion, pca_proportion)
    data_final_df = pd.DataFrame(data_final_array)
    data_final_df = data_final_df.reset_index(drop=True)
    data_y = data_y.reset_index(drop=True)
    data_day_code = data_day_code.reset_index(drop=True)

    formal_data = pd.concat([data_day_code, data_final_df, data_y], axis=1)
    return formal_data


def main_data_process(
        prop_train_day,
        prop_valid_day,
        prop_test_day,
        y_clusters_num,
        corr_proportion,
        lda_proportion,
        pca_proportion,
        filename):
    data = clean_and_reduct_data(
        prop_test_day,
        y_clusters_num,
        corr_proportion,
        lda_proportion,
        pca_proportion,
        filename)
    print('clean_and_reduct_data have been done!!!wait a moment!!!')
    train_data = data[data['TradingDay'] <= prop_train_day]
    valid_data = data[(data['TradingDay'] > prop_train_day)
                      & ((data['TradingDay'] <= prop_valid_day))]
    test_data = data[(data['TradingDay'] > prop_valid_day)
                     & ((data['TradingDay'] <= prop_test_day))]
    res = [train_data, valid_data, test_data]
    train_data.to_csv(
        'E:/huangpeng/acknowledge/huang_peng/data/factor_for_deeplearning/'
        'train_data.csv')
    valid_data.to_csv(
        'E:/huangpeng/acknowledge/huang_peng/data/factor_for_deeplearning/'
        'valid_data.csv')
    test_data.to_csv(
        'E:/huangpeng/acknowledge/huang_peng/data/factor_for_deeplearning/'
        'test_data.csv')
    return res


# -----------------------------------------------------------------------------
# TEST
# x = x[:, 0]
# my = DataCleaning()
# ccc = my._kick_extreme_mad_tanh1(x)
#
# cccccc = 1
# x = pd.DataFrame(x)
# b = x.where(x == 1)
#
# # my = DataDimension()
# # ccc = my.sort_abs_corr(x)
# # ar = ccc.values
# # larg_index = np.argsort(x)
# ccc = get_position(1, x)
# aaa = list(ccc)[1]
# b = 1
# rows = 6
# columns = 20
# np.random.seed(1234)  # 设置随机种子为1234
# rand = np.random.randint(-2, 2, [rows, columns])
# data = pd.DataFrame(rand)
# triu_matrix = abs_corr_triumatrix(data, method='pearson')
# corr_nonzero_list = sort_abs_corr(triu_matrix)
# n_dimension = 16
# feature_ic_asb_average = rand = np.random.rand(1, columns).tolist()
# feature_ic_asb_average = feature_ic_asb_average[0]
#
# res = dimen_reduct_corr(data,
#                         triu_matrix,
#                         corr_nonzero_list,
#                         n_dimension,
#                         feature_ic_asb_average)
# # ccc = 1
# del_list=[0,1,6,8]
# rows = 6
# columns = 20
# rand = np.random.randint(-2, 2, [rows, columns])
# df = pd.DataFrame(rand)
# c = df.columns
# d = list(c)
# # 挑选出希望被删除的列的名称的集合
# e = [d[i] for i in del_list]
# f=df.drop(e,axis=1)
# ccc=1
# *y_clusters_num must above corr_proportion*lda_proportion*sample_num
# np.random.seed(1234)  # 设置随机种子为1234
# rows = 20
# columns = 28
# rand = np.random.randint(-2, 2, [rows, columns])
# data = pd.DataFrame(rand)
# res = abs_icir(data)
# ccc = 1

# Establish the data after dimension reduction.
main_data_process(
    prop_train_day=150,
    prop_valid_day=200,
    prop_test_day=250,
    y_clusters_num=200,
    corr_proportion=0.7,
    lda_proportion=0.3,
    pca_proportion=0.5,
    filename='E:/huangpeng/acknowledge/huang_peng/data/factor_for_deeplearning/insample_data_360.hdf5')
