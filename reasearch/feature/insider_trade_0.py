import numpy as np
import pandas as pd
from datetime import date
import copy
from sklearn.linear_model import LinearRegression


"""
基于海通证券 选股因子系列研究（五十八）——知情交易与主买主卖
"""


def get_insider_trade_0(
    minute_price_data: pd.DataFrame,
    start_date: int,
    end_date: int
) -> pd.DataFrame:
    """
    The main function used to create factor.

    Parameters
    ----------
    minute_price_data: pandas DataFrame
        dataframe with columns: 'TradingDay', 'SecuCode', 'Time',
        'OpenPrice', 'HighPrice', 'LowPrice', 'ClosePrice','Turnover',
        'Volume'
        Tips: TradingDay is Ascending and continuous Time is ascending and
        continuous, index is unique.
    start_date: int
    end_date: int

    Returns
    -------
    pandas DataFrame with columns:
        'TradingDay', 'SecuCode', and columns for factor exposures
    """
    # replace nan by ffillna or bfillna
    minute_price_data = minute_price_data.fillna(method='bfill')
    minute_price_data = minute_price_data.fillna(method='ffill')
    # Selects data at a specified time
    minute_price_data = minute_price_data[
        minute_price_data['TradingDay'] >= start_date]
    minute_price_data = minute_price_data[
        minute_price_data['TradingDay'] <= end_date]
    minute_price_data = minute_price_data.loc[:, [
        'TradingDay', 'Time',
        'Return', 'Turnover',
        'Volume']]
    # Set up parameters
    open_time = 930
    close_time = 1427
    reg_window = 20
    """ The processed data is obtained, including the addition of dummy
    variables and residual """
    data_for_cal = get_unexpected_return(minute_price_data,
                                         reg_window)
    diff_day = list(np.unique(data_for_cal['TradingDay']))
    allday_res = []
    for selected_day in diff_day:
        today_data = data_for_cal[
            data_for_cal['TradingDay'] == selected_day]
        allperiod_res = [selected_day]
        for period in ['allday', 'front', 'middle', 'end']:
            if period == 'allday':
                tempdata = today_data
            elif period == 'front':
                tempdata = today_data[
                    (today_data['Time'] >= open_time) & (
                        today_data['Time'] <= open_time + 29)]
            elif period == 'middle':
                tempdata = today_data[
                    (today_data['Time'] >= open_time + 30) & (
                        today_data['Time'] <= close_time - 1)]
            else:
                tempdata = today_data[
                    (today_data['Time'] >= close_time) & (
                        today_data['Time'] <= close_time + 29)]
            insider_buy_rate = cal_insider_buy(tempdata)
            insider_sell_rate = cal_insider_sell(tempdata)
            insider_netbuy_rate = insider_buy_rate - insider_sell_rate
            allperiod_res.append(insider_buy_rate)
            allperiod_res.append(insider_sell_rate)
            allperiod_res.append(insider_netbuy_rate)
        allday_res.append(allperiod_res)
    res = pd.DataFrame(allday_res)
    res.columns = ['TradingDay', 'AlldayBuy', 'AlldaySell', 'AlldayNet',
                   'FrontBuy', 'FrontSell', 'FrontNet', 'MiddleBuy',
                   'MiddleSell', 'MiddleNet', 'EndBuy', 'EndSell', 'EndNet']

    return res


def cal_insider_buy(oneday_period_data: pd.DataFrame) -> float:
    """
    Calculate the proportion of informed purchase.

    Parameters
    ----------
    oneday_period_data: pandas DataFrame
        dataframe with columns: 'Turnover', 'residual'

    Returns
    -------
    float
    """
    all_turnover = oneday_period_data['Turnover'].sum()
    insider_buy = oneday_period_data[
        oneday_period_data['residual'] <= 0]
    buy_all_turnover = insider_buy['Turnover'].sum()
    if all_turnover == 0:
        res = 0
    else:
        res = buy_all_turnover / all_turnover
    return res


def cal_insider_sell(oneday_period_data: pd.DataFrame) -> float:
    """
    Calculate the proportion of informed selling.

    Parameters
    ----------
    oneday_period_data: pandas DataFrame
        dataframe with columns: 'Turnover', 'residual'

    Returns
    -------
    float
    """
    all_turnover = oneday_period_data['Turnover'].sum()
    insider_sell = oneday_period_data[
        oneday_period_data['residual'] > 0]
    sell_all_turnover = insider_sell['Turnover'].sum()
    if all_turnover == 0:
        res = 0
    else:
        res = sell_all_turnover / all_turnover
    return res


def get_unexpected_return(
        minute_price_data: pd.DataFrame,
        reg_windows: int) -> pd.DataFrame:
    """
    Residuals were obtained by regression of the data within the time window.
    Tips: The time point 9:31 is deleted because there is no data for previous
    moments.

    Parameters
    ----------
    minute_price_data: pandas DataFrame
        dataframe with columns: 'TradingDay', 'SecuCode', 'Time',
        'OpenPrice', 'HighPrice', 'LowPrice', 'ClosePrice', 'Turnover'
        , 'Volume'
    reg_windows： int

    Returns
    -------
    pandas DataFrame with columns:
        'TradingDay', 'Time', 'Return', 'Turnover', 'Volume', 'front',
        'middle','end', 'w1', 'w2', 'w3', 'w4', 'previous_day_revenue_rate',
        'residual'
    """
    data_with_binary = add_proxy_var(minute_price_data)
    data_with_binary = data_with_binary[
        data_with_binary['Time'] != 931]
    this_miu_data = data_with_binary
    this_miu_data = this_miu_data.reset_index(drop=True)
    diff_day = list(np.unique(this_miu_data['TradingDay']))
    # Do regression within each time window
    if len(diff_day) <= reg_windows:
        raise Exception('length is short than regression windows')
    alldata = []
    for day in range(len(diff_day) - reg_windows + 1):
        start = diff_day[day]
        end = diff_day[day + reg_windows - 1]
        # print('regression day', end)
        window_data = this_miu_data[
            (this_miu_data['TradingDay'] >= start) & (
                this_miu_data['TradingDay'] <= end)]
        window_data = window_data.reset_index(drop=True)
        window_data = window_data.reset_index(drop=True)
        x = window_data.loc[:,
                            ['w1', 'w2', 'w3', 'w4', 'front', 'middle', 'end',
                             'previous_day_revenue_rate']]
        x = x.values
        y = window_data.loc[:, ['Return']]
        y = y.values
        residual_df = cal_residual(x, y)
        residual_df = residual_df.reset_index(drop=True)
        temp = pd.concat([window_data, residual_df], axis=1,
                         join='inner')
        temp_today = temp[temp['TradingDay'] == end]
        temp_today_list = temp_today.values.tolist()
        alldata.append(temp_today_list)
    # Eliminate the outer brackets
    alldata = [i for j in alldata for i in j]
    res = pd.DataFrame(alldata)
    res.columns = ['TradingDay', 'Time', 'Return', 'Turnover',
                   'Volume', 'front', 'middle', 'end', 'w1', 'w2', 'w3',
                   'w4', 'previous_day_revenue_rate', 'residual']
    # res.to_csv('data_with_unexpected_return.csv')
    return res


def cal_residual(x: np.ndarray, y: np.ndarray) -> pd.DataFrame:
    """
    The residuals were calculated by linear regression.

    Parameters
    ----------
    x: numpy Ndarray
    y: numpy Ndarray

    Returns
    -------
    pandas DataFrame with columns:
    """
    reg = LinearRegression().fit(x, y)
    residual = y - reg.predict(x)
    residual_df = pd.DataFrame(residual)
    return residual_df


def add_proxy_var(ini_data: pd.DataFrame) -> pd.DataFrame:
    """
    Add dummy variables for time period and week in DataFrame.

    Parameters
    ----------
    ini_data: pandas DataFrame
        dataframe with columns: 'Time','TradingDay',...

    Returns
    -------
    pandas DataFrame with columns:
        'Time','TradingDay','front', 'middle', 'end','w1', 'w2', 'w3',
        'w4', 'previous_day_revenue_rate'...
    """
    ini_data = ini_data.reset_index(drop=True)
    data = copy.deepcopy(ini_data)
    data = data.reset_index(drop=True)
    time_series = data['Time']
    weekday_series = data['TradingDay']
    time_binary = time_series.apply(check_days_period)
    time_binary = time_binary.tolist()
    time_binary_df = pd.DataFrame(time_binary)
    time_binary_df.columns = ['front', 'middle', 'end']
    time_binary_df = time_binary_df.reset_index(drop=True)

    weekday_binary = weekday_series.apply(check_weekday)
    weekday_binary = weekday_binary.tolist()
    weekday_binnary_df = pd.DataFrame(weekday_binary)
    weekday_binnary_df.columns = ['w1', 'w2', 'w3', 'w4']
    weekday_binnary_df = weekday_binnary_df.reset_index(drop=True)

    previous_reven = get_previous_revenue_rate(ini_data)
    previous_reven_df = previous_reven.to_frame()
    previous_reven_df.columns = ['previous_day_revenue_rate']
    previous_reven_df = previous_reven_df.reset_index(drop=True)
    res = pd.concat([ini_data,
                     time_binary_df,
                     weekday_binnary_df,
                     previous_reven_df],
                    axis=1,
                    join='inner')
    return res


# kits
def get_previous_revenue_rate(ini_data: pd.DataFrame) -> pd.Series:
    """
    Generates a Series of returns at the previous time point.
    Tips: There is no  yield data at 9:31, fill it with empty np.nan.

    Parameters
    ----------
    ini_data: pandas DataFrame
        dataframe with columns: 'Return',...

    Returns
    -------
    pandas Series
    """
    revenue = ini_data['Return']
    pre_reven = revenue[0:-1]
    s = pd.Series(np.nan)
    res = s.append(pre_reven, ignore_index=True)
    return res


def check_weekday(tradingday: float) -> list:
    """
    Check the day of the week for the current date.

    Parameters
    ----------
    tradingday: float
        float with example:20100101.0, 01990101.0

    Returns
    -------
    list with example:
        {[1, 0, 0, 0]:Monday, [0, 1, 0, 0]:Tuesday, [0, 0, 1, 0]:
        Wednesday, [0, 0, 0, 1]:Thursday, [0, 0, 0, 0]:Else}
    """
    str_day = str(tradingday)
    year = str_day[:4]
    month = str_day[4:6]
    day = str_day[6:]
    a = date(int(year), int(month), int(day))
    res = a.weekday()
    if res == 0:
        return [1, 0, 0, 0]
    elif res == 1:
        return [0, 1, 0, 0]
    elif res == 2:
        return [0, 0, 1, 0]
    elif res == 3:
        return [0, 0, 0, 1]
    else:
        return [0, 0, 0, 0]


def check_days_period(time: float) -> list:
    """
    Check which period the current time belongs to.

    Parameters
    ----------
    time: float
        float with example:930.0, 960.0, 1260.0

    Returns
    -------
    list with example:
        {[1, 0, 0]:Front, [0, 1, 0]:Middle, [0, 0, 1]:End, [0, 0, 0]:Else}
    """
    open_time = 930
    close_time = 1427
    if (time >= open_time) & (time <= open_time + 29):
        return [1, 0, 0]
    elif (time >= close_time) & (time <= close_time + 29):
        return [0, 0, 1]
    elif (time >= open_time + 30) & (time <= close_time - 1):
        return [0, 1, 0]
    else:
        return [0, 0, 0]


# function for test
def program_test():
    """
    For test

    Returns
    -------

    """
    ccc = get_test_data()
    # ccc.reset_index(drop=True)
    get_insider_trade_0(ccc, 19900000, 22220202)
    # program_test()
    return 0


def get_test_data() -> pd.DataFrame:
    """
    Obtain data for test

    Returns
    -------
    pandas DataFrame with columns:
        'TradingDay', 'SecuCode', 'Time','OpenPrice', 'HighPrice',
        'LowPrice', 'ClosePrice','Turnover', 'Volume'
    """
    dataframe = pd.read_hdf('../../data/insider_trade/000029.h5')
    dataframe = dataframe.fillna(method='bfill')
    dataframe = dataframe.fillna(method='ffill')
    # dataframe = dataframe[dataframe['TradingDay'] > 20191201]
    dataframe = dataframe.reset_index(drop=True)
    return dataframe
